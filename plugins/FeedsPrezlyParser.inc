<?php
/**
 * @file
 * Prezly Parser plugin.
 */

/**
 * Feeds parser plugin that parses Prezly JSON feeds.
 */
class FeedsPrezlyParser extends FeedsParser {

  /**
   * Implements FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $data = $fetcher_result->getRaw();

    if ($data['items']) {
      $data['items'] = $this->_convertSourceArraysWithDelimiter($data['items']);
    }

    $result = new FeedsParserResult($data['items']);
    $result->title = $data['title'];
    $result->link = $data['link'];

    return $result;
  }

  /**
   * Convert the source array so source mapping fields with delimiters are simplified so they become compatible
   * with feeds mapping callbacks.
   *
   * @param array $items
   *   The source items array.
   * @param string $delimiter
   *   The delimiter, by default ':'.
   *
   * @return array
   *   The converted array.
   */
  protected function _convertSourceArraysWithDelimiter(array $items, $delimiter = ':') {
    $cleanup = array();
    foreach ($this->_getDelimiterMappingSources($delimiter) as $source_key) {
      list($property, $attribute) = explode($delimiter, $source_key);
      foreach ($items as &$item) {
        if (isset($item[$property])) {
          if (isset($item[$property][$attribute])) {
            $item[$property . $delimiter . $attribute] = $item[$property][$attribute];
          }
          elseif (is_array($item[$property])) {
            $item[$property . $delimiter . $attribute] = array();
            foreach ($item[$property] as $sub_item) {
              if (isset($sub_item[$attribute])) {
                $item[$property . $delimiter . $attribute][] = $sub_item[$attribute];
              }
            }
          }
        }
      }
      if (!isset($cleanup[$property])) {
        $cleanup[$property] = $property;
      }
    }

    // Cleanup (old) converted arrays.
    foreach ($items as $key => &$item) {
      $item = array_diff_key($item, $cleanup);
    }

    return $items;
  }

  /**
   * Get all mapping sources containing a delimiter as key.
   *
   * @param string $delimiter
   *   The delimiter, by default ':'.
   *
   * @return array
   *   A list of all mapping sources that contain a delimiter.
   */
  protected function _getDelimiterMappingSources($delimiter = ':') {
    return array_filter(array_keys($this->getMappingSources()), function ($value) use ($delimiter) {
      return (strpos($value, $delimiter) !== FALSE);
    });
  }

  /**
   * Return mapping sources.
   */
  public function getMappingSources() {
    return array(
      // Feed source fields.
      'feed_title' => array(
        'name' => t('Feed title'),
        'description' => t('Title of the feed.'),
      ),
      'feed_url' => array(
        'name' => t('Feed URL'),
        'description' => t('URL of the feed.'),
      ),
      // Item source fields.
      'title' => array(
        'name' => t('Title (item)'),
        'description' => t('Title of an item.'),
      ),
      'subtitle' => array(
        'name' => t('Subtitle (item)'),
        'description' => t('Subtitle of an item.'),
      ),
      'preview_image' => array(
        'name' => t('Preview image (item)'),
        'description' => t('Preview image of an item.'),
        'callback' => 'feeds_prezly_get_file_uri',
      ),
      'intro' => array(
        'name' => t('Intro (item)'),
        'description' => t('Intro of an item.'),
      ),
      'url' => array(
        'name' => t('URL (item)'),
        'description' => t('URL of an item.'),
      ),
      'modified_at' => array(
        'name' => t('Modified date (item)'),
        'description' => t('Modified at date of an item.'),
        'callback' => 'feeds_prezly_get_datetime_element',
      ),
      'published_at' => array(
        'name' => t('Published date (item)'),
        'description' => t('Published at date of an item.'),
        'callback' => 'feeds_prezly_get_datetime_element',
      ),
      'language' => array(
        'name' => t('Language (item)'),
        'description' => t('The language of an item.'),
      ),
      'contacts:name' => array(
        'name' => t('Contacts name (item)'),
        'description' => t('A list of names of contacts of an item.'),
      ),
      'contacts:email' => array(
        'name' => t('Contacts e-mail (item)'),
        'description' => t('A list of e-mail addresses of contacts of an item.'),
      ),
      'contacts:description' => array(
        'name' => t('Contacts description (item)'),
        'description' => t('A list of descriptions of contacts of an item.'),
      ),
      'contacts:telephone' => array(
        'name' => t('Contacts telephone (item)'),
        'description' => t('A list of telephones of contacts of an item.'),
      ),
      'contacts:company' => array(
        'name' => t('Contacts company (item)'),
        'description' => t('A list of companies of contacts of an item.'),
      ),
      // Item detail source fields (requires enabling fetch_details in Prezly fetcher).
      'short_url' => array(
        'name' => t('Short URL (item detail)'),
        'description' => t('Short URL of an item.'),
      ),
      'body' => array(
        'name' => t('Body (item detail)'),
        'description' => t('Body of an item.'),
      ),
      'mainvisual:thumbnail' => array(
        'name' => t('Main visual:thumbnail (item detail)'),
        'description' => t('Thumbnail version of main visual of an item.'),
        'callback' => 'feeds_prezly_get_file_uri',
      ),
      'mainvisual:large' => array(
        'name' => t('Main visual:large (item detail)'),
        'description' => t('%size version of the main visual of an item.', array('%size' => t('Large'))),
        'callback' => 'feeds_prezly_get_file_uri',
      ),
      'mainvisual:original' => array(
        'name' => t('Main visual:original (item detail)'),
        'description' => t('%size version of the main visual of an item.', array('%size' => t('Original'))),
        'callback' => 'feeds_prezly_get_file_uri',
      ),
      'header:large' => array(
        'name' => t('Header:large (item detail)'),
        'description' => t('Large version of the header of an item.'),
        'callback' => 'feeds_prezly_get_file_uri',
      ),
      'header:release' => array(
        'name' => t('Header:release (item detail)'),
        'description' => t('Release version of the header of an item.'),
        'callback' => 'feeds_prezly_get_file_uri',
      ),
      'videos:caption' => array(
        'name' => t('Videos:caption (item detail)'),
        'description' => t('A list of captions for the videos of an item.'),
      ),
      'videos:thumbnail' => array(
        'name' => t('Videos:thumbnail (item detail)'),
        'description' => t('A list of thumbnails for the videos of an item.'),
      ),
      'videos:url' => array(
        'name' => t('Videos:url (item detail)'),
        'description' => t('A list of urls for the videos of an item.'),
      ),
      'videos:embed' => array(
        'name' => t('Videos:embed (item detail)'),
        'description' => t('A list of embed codes for the videos of an item.'),
      ),
      'images:caption' => array(
        'name' => t('Images:caption (item detail)'),
        'description' => t('A list of captions for the images of an item.'),
      ),
      'images:original' => array(
        'name' => t('Images:original (item detail)'),
        'description' => t('A list of %size versions of the images of an item.', array('%size' => t('original'))),
        'callback' => 'feeds_prezly_get_file_uri',
      ),
      'images:medium' => array(
        'name' => t('Images:medium (item detail)'),
        'description' => t('A list of %size versions of the images of an item.', array('%size' => t('medium'))),
        'callback' => 'feeds_prezly_get_file_uri',
      ),
      'images:small' => array(
        'name' => t('Images:small (item detail)'),
        'description' => t('A list of %size versions of the images of an item.', array('%size' => t('small'))),
        'callback' => 'feeds_prezly_get_file_uri',
      ),
      'images:thumbnail' => array(
        'name' => t('Images:thumbnail (item detail)'),
        'description' => t('A list of %size versions of the images of an item.', array('%size' => t('thumbnail'))),
        'callback' => 'feeds_prezly_get_file_uri',
      ),
      'images:square' => array(
        'name' => t('Images:square (item detail)'),
        'description' => t('A list of %size versions of the images of an item.', array('%size' => t('square'))),
        'callback' => 'feeds_prezly_get_file_uri',
      ),
      'images:squaresmall' => array(
        'name' => t('Images:squaresmall (item detail)'),
        'description' => t('A list of %size versions of the images of an item.', array('%size' => t('squaresmall'))),
        'callback' => 'feeds_prezly_get_file_uri',
      ),
      'links:description' => array(
        'name' => t('Links:description (item detail)'),
        'description' => t('A list of descriptions for links of an item.'),
      ),
      'links:url' => array(
        'name' => t('Links:url (item detail)'),
        'description' => t('A list of urls for links of an item.'),
      ),
      'attachments:description' => array(
        'name' => t('Attachments:description (item detail)'),
        'description' => t('A list of descriptions for attachments of an item.'),
      ),
      'attachments:extension' => array(
        'name' => t('Attachments:extension (item detail)'),
        'description' => t('A list of extensions for attachments of an item.'),
      ),
      'attachments:url' => array(
        'name' => t('Attachments:url (item detail)'),
        'description' => t('A list of urls for attachments of an item.'),
        'callback' => 'feeds_prezly_get_file_uri',
      ),
      'tags' => array(
        'name' => t('Tags (item detail)'),
        'description' => t('A list of tags of an item.'),
      ),
    ) + parent::getMappingSources();
  }
}

/**
 * Prezly enclosure element, can be part of the result array.
 */
class PrezlyFeedsEnclosure extends FeedsEnclosure {
  /**
   * Constructor, requires MIME type.
   *
   * @param string $value
   *   A path to a local file or a URL to a remote document.
   * @param string $mime_type
   *   The mime type of the resource.
   */
  public function __construct($value, $mime_type) {
    parent::__construct($value, $mime_type);
  }

  /**
   * Downloads the content from the file URL.
   *
   * @return string
   *   The content of the referenced resource.
   *
   * @throws Exception
   */
  public function getContent() {
    feeds_include_library('http_request.inc', 'http_request');
    $result = drupal_http_request($this->getUrlEncodedValue());
    if ($result->code != 200) {
      throw new Exception(t('Download of @url failed with code !code.', array('@url' => $this->getUrlEncodedValue(), '!code' => $result->code)));
    }
    return $result->data;
  }

  /**
   * Get a Drupal file object of the enclosed resource, download if necessary.
   *
   * @param string $destination
   *   The path or uri specifying the target directory in which the file is
   *   expected. Don't use trailing slashes unless it's a streamwrapper scheme.
   *
   * @return object
   *   A Drupal temporary file object of the enclosed resource.
   *
   * @throws Exception
   *   If file object could not be created.
   */
  public function getFile($destination) {
    // Look for existing file object.
    $file = feeds_prezly_file_load_by_uri($this->getValue());
    if (!$file) {
      // Not found, create a new one.
      $file = feeds_prezly_file_create_by_uri($this->getValue());
    }

    // Validate file & save.
    // TODO consider moving to new file and only validating extension here...
    $file->uid = 0;
    $file->uri = $this->getSanitizedUri();
    $file->filemime = $this->getMIMEType();
    $file->filename = $this->getSafeFilename();
    file_save($file);

    // We couldn't make sense of this enclosure, throw an exception.
    if (!$file) {
      throw new Exception(t('Invalid enclosure %enclosure', array('%enclosure' => $this->getValue())));
    }
    return $file;
  }
}
