<?php
/**
 * @file
 * Contains FeedsPrezlyFetcher class.
 */

/**
 * Definition of the import batch object created on the fetching stage by FeedsPrezlyFetcher.
 */
class FeedsPrezlyFetcherResult extends FeedsFetcherResult {

  /**
   * The URL of the feed being fetched.
   *
   * @var string
   */
  protected $url;

  /**
   * TRUE if press release details should be fetched, FALSE if not.
   *
   * @var boolean
   */
  protected $fetch_details;

  /**
   * Constructor.
   */
  public function __construct($press_room, $fetch_details = FALSE) {
    $this->url = self::getFeedUrl($press_room);
    $this->fetch_details = $fetch_details;
  }

  /**
   * Overrides FeedsFetcherResult::getRaw();
   */
  public function getRaw() {
    feeds_include_library('http_request.inc', 'http_request');

    $response = http_request_get($this->url);
    if ($response->code != 200) {
      throw new Exception(t('Download of @url failed with code !code.', array('@url' => $this->url, '!code' => $response->code)));
    }

    if (($data = drupal_json_decode($response->data)) !== NULL) {
      if ($this->fetch_details) {
        foreach ($data['items'] as &$item) {
          // TODO http caching... compare updated_at timestamp ?
          $item_url = self::getItemDetailFeedUrl($item['url']);

          $details_response = http_request_get($item_url);
          if ($details_response->code != 200) {
            throw new Exception(t('Download of @url failed with code !code.', array('@url' => $item_url, '!code' => $details_response->code)));
          }

          $details_data = drupal_json_decode($details_response->data);
          if ($details_data !== NULL) {
            // Add extra retrieved details data to $item.
            $item += $details_data;
          }
          else {
            throw new Exception(t('JSON parsing of @url failed.', array('@url' => $item_url)));
          }
        }
      }
      return (array) $data;
    }
    throw new Exception(t('JSON parsing of @url failed.', array('@url' => $this->url)));
  }

  /**
   * Get the main feed url.
   *
   * @param string $press_room
   *   The name of the press room.
   *
   * @return string
   *   The feed url.
   */
  public static function getFeedUrl($press_room) {
    return "http://{$press_room}.prezly.com/feed.json";
  }

  /**
   * Get the item detail feed url.
   *
   * @param string $url
   *   The item url.
   *
   * @return string
   *   The feed url.
   */
  public static function getItemDetailFeedUrl($url) {
    return "{$url}.json";
  }
}

/**
 * A parser for the Prezly press releases.
 *
 * @see http://docs.prezly.com/v1.0/docs/1-publishing-press-releases-on-your-website-via-th
 */
class FeedsPrezlyFetcher extends FeedsFetcher {

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    $config = $this->getConfig();
    return new FeedsPrezlyFetcherResult($source_config['press_room'], $config['fetch_details']);
  }

  /**
   * Implements FeedsFetcher::clear().
   */
  public function clear(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    $url = FeedsPrezlyFetcherResult::getFeedUrl($source_config['press_room']);
    feeds_include_library('http_request.inc', 'http_request');
    http_request_clear_cache($url);
  }

  /**
   * Implements FeedsFetcher::request().
   */
  public function request($feed_nid = 0) {
    feeds_dbg($_GET);
    @feeds_dbg(file_get_contents('php://input'));

    try {
      feeds_source($this->id, $feed_nid)->existing()->import();
    }
    catch (Exception $e) {
      // In case of an error, respond with a 503 Service (temporary) unavailable.
      header('HTTP/1.1 503 "Not Found"', NULL, 503);
      drupal_exit();
    }

    // Will generate the default 200 response.
    header('HTTP/1.1 200 "OK"', NULL, 200);
    drupal_exit();
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'fetch_details' => 0,
    );
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = array();

    $form['fetch_details'] = array(
      '#type' => 'checkbox',
      '#title' => t('Fetch details'),
      '#description' => t('Fetch details of press releases. If enabled, more source fields will be available for mapping.'),
      '#default_value' => $this->config['fetch_details'],
    );

    return $form;
  }

  /**
   * Override parent::sourceForm().
   */
  public function sourceForm($source_config) {
    $form = array();

    $form['press_room'] = array(
      '#type' => 'textfield',
      '#title' => t('Press room'),
      '#description' => t('Enter Prezly press room name. eg http://!name.prezly.com/', array('!name' => '<strong>example-name</strong>')),
      '#default_value' => isset($source_config['press_room']) ? $source_config['press_room'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );

    return $form;
  }

  /**
   * Override parent::sourceFormValidate().
   */
  public function sourceFormValidate(&$values) {
    if (!preg_match('/^[a-zA-Z0-9\-_]+$/', $values['press_room'])) {
      $form_key = 'feeds][' . get_class($this) . '][press_room';
      form_set_error($form_key, t('The press room name must contain only lowercase letters, numbers, and hyphens.'));
    }
  }
}
